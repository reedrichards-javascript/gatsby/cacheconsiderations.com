/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"

import Header from "./header"
import "./layout.css"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <>
      <div>
        <div
          style={{
            margin: `0 auto`,
            padding: `0px 1.0875rem 1.45rem`,
            paddingTop: 0,
          }}
        >
          <h1>Cache Considerations</h1>
          <nav>
            <ol>
              <li>
                <a href="/">Home</a>
              </li>
              <li>
                <a href="bio">Bio</a>
              </li>
              {/* <li> */}
              {/*   <a href="https://blog.cacheconsiderations.com">Blog</a> */}
              {/* </li> */}
            </ol>
          </nav>
          <main>{children}</main>
          <div
            style={{
              borderTop: "double",
            }}
          >
            <ul
              style={{
                listStyle: "none",
                paddingLeft: 0,
              }}
            >
              <li>© Robert Wendt {new Date().getFullYear()}</li>
              <li>
                <a href="https://gitlab.com/reedrichards">Gitlab</a>
              </li>
              <li>
                <a href="https://www.linkedin.com/in/robert-wendt-150239186/">
                  Linkedin
                </a>
              </li>
              <li>
                <a href="https://resume.cacheconsiderations.com">Resume</a>
              </li>
              <li>
                <a href="mailto:rwendt1337@gmail.com">rwendt1337@gmail.com</a>
              </li>
              <li>
                Built with {` `}
                <a href="https://www.gatsbyjs.org">Gatsby</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
