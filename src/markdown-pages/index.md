---
path: "/"
title: "Home"
---

Welcome 💸 🤔

The name of this site is is a play on words for <a href="https://www.quora.com/NBA-trades-sometimes-involve-cash-considerations-What-exactly-does-it-mean-and-how-does-it-effect-the-salary-cap">Cash Considerations</a>. This domain and it's subdomains act as public facing resources for various services and projects that I have.

## Notable Projects

### <a href="https://g.cacheconsiderations.com">Grimoire</a>

A [Grimoire](https://en.wikipedia.org/wiki/Grimoire) is a book of spells, typically including instructions on how to create magical objects such as talismans and amulets. My grimoire is similar, it contains instructions on how to create magical software and useful code snippets. It also includes more general notes on health, interesting quotes, or whatever else I feel like jotting down.

### [Gitlab Helper](https://reedrichards.gitlab.io/gitlab-helper/readme.html)

**Work in progress**. Gitlab Helper is a slowly growing collection of scripts and actions that help me automate my workflows with gitlab written in python. It aims to have a similar syntax as kubectl and someday yaml config for settings and command definitions.

### [flatten-array-ts](https://reedrichards-javascript.gitlab.io/typescript/flatten-array)

a typescript module for flattening an array. Also practice for setting up CI/CD pipelines for typescript modules on gitlab.

### [log-redactor](https://gitlab.com/reedrichards-python/log-redactor)

a python logging formatter to remove a pattern from your logs.

### [text2md5sum](https://gitlab.com/reedrichards-javascript/react/text2md5sum)

generates an md5sum from text, written in React.js

### [gHost](https://gitlab.com/reedrichards/ghost)

an in memory file server written in go. designed to be easily deployed on to kubernetes and forgotten about. Originally written to serve chess pgn files quickly to various clients to do analytics. Documentation needs to be updated.


### [layer-d8](https://gitlab.com/reedrichards/layer-d8)

An irc bot written in Go. Responds to `,date` and return's the current date in the format of `Mon 2006-01-2`. I created this in response to this not being a function in the irc channel `#reddit-sysadmin` on freenode, but the channel moderators where not fans so it doesn't exist there anymore. It's name is a play on words for `layer-eight`, another friend of ours in that channel.

### [sync-lichess](https://gitlab.com/reedrichards/sync-lichess)

program to sync your lichess pgn's to a directory written in Go. README has instructions for deployment to kubernetes.

### [lichess](https://gitlab.com/reedrichards/lichess)

incomplete lichess api client written in Go. Used for [sync-lichess](https://gitlab.com/reedrichards/sync-lichess).

### [yas-django](https://gitlab.com/reedrichards/yas-django)

lots of [yasnippets](https://github.com/joaotavora/yasnippet) for django for emacs.

### [negative-nancy](https://gitlab.com/reedrichards/negative-nancy)

kind of a childish IRC bot written in go. There was an irc channel that I was part of that had a feature where you could vote on things by saying the name of something and then adding `++` or `--` after it. So if I wanted to upvote golang, I would enter `golang ++` and then a bot would respond with `golang now has 1 karma`. I thought it would be funny to downvote everything that ever would be voted on with a bot named negative-nancy because I mean, come on right? Needless to say, `negative-nancy` has been retired but it was a nice learning experience.

### [angel-apply](https://gitlab.com/reedrichards/angel-apply)

batch apply for jobs on [Angelist](angel.co) written with Selenium Python and some javascript. Use this if you want to get shadowbanned like I did.


### [resume](https://gitlab.com/reedrichards-javascript/gatsby/resume)
resume using [Gatsby](https://www.gatsbyjs.org/), a static site generator using React.js and Graphql sprinkled with typescript. I barely take advantage of any of those things, but this allows me to generate customized resume's fairly easily since I'm really only editing a markdown file.

### [cacheconsiderations.com](https://gitlab.com/reedrichards-javascript/gatsby/cacheconsiderations.com)

This website! Made using [Gatsby](https://www.gatsbyjs.org/), a static site generator using React.js and Graphql.
